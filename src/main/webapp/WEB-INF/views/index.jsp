<%@ include file="/WEB-INF/views/_taglibs.jspf"%>
<div class="container">
	<div class="page-header">
		<h1><fmt:message key="welcome.title" /></h1>
	</div>
	<p class="lead"><fmt:message key="welcome.caption" /></p>
</div>