<%@ include file="/WEB-INF/views/_taglibs.jspf"%>
<div class="container">
    <h1> ${movie.movieName}</h1>
    <h3> Theater ${theatre.number } ${showing.timestamp }</h3>
    
    <table border="1">
    <c:set var="ctr" value="0" scope="page"/>
    <c:forEach begin="0" end="${theatre.getMaxY() - 1}" varStatus="y">
        <tr>
		<c:forEach begin="0" end="${theatre.getMaxX() - 1}" varStatus="x">
			<td width=20 height=20  align="center">
                <c:if test="${!seats[ctr].available}">
					-
				</c:if>
                <c:if test="${seats[ctr].available}">
                    <c:if test="${seats[ctr].occupied }">
						<form:form action="/movies/${movie.id}/${showing.id}/ticket" method="post" modelAttribute="seat">
							<form:hidden path="x" value="${x.index}"/>
							<form:hidden path="y" value="${y.index}"/>
							<form:hidden path="theatre" value="${theatre}"/>
							<input type="submit" value="X">
						</form:form>
                    </c:if>
                    <c:if test="${!seats[ctr].occupied }">
						<form:form action="/movies/${movie.id}/${showing.id}/buy" method="post" modelAttribute="seat">
							<form:hidden path="x" value="${x.index}"/>
							<form:hidden path="y" value="${y.index}"/>
							<form:hidden path="theatre" value="${theatre}"/>
							<input type="submit" value="O">
						</form:form>
                    </c:if>
                </c:if>
			<c:set var="ctr" value="${ctr + 1}" scope="page"/>
			</td>
		</c:forEach>
		</tr>
    </c:forEach>
    <tr><td colspan="${theatre.getMaxX()}" align="center">SCREEN</td></tr>
	</table>
</div>