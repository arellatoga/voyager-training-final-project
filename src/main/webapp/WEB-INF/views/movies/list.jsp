<%@ include file="/WEB-INF/views/_taglibs.jspf"%>
<div class="container">
	<div class="row">
	<h1><fmt:message key="movies.list.title" /></h1>
	<table class="table">
		<thead>
			<tr>
				<th>
					<fmt:message key="label.Movie.number"/>
				</th>
				<th>
					<fmt:message key="label.Movie.schedule"/>
				</th>
			</tr>
		</thead>
		<c:forEach var="movie" items="${movies}">
			<tr>
				 <td>${movie.movieName} - ${movie.runtime} minutes
				</td>
				<td>
				    <c:forEach var="showing" items="${movie.showings}">
				        <spring:url var="schedUrl" value="movies/{movieId}/{scheduleId}">
				            <spring:param name="movieId" value="${movie.getId()}"/>
				            <spring:param name="scheduleId" value="${showing.id}"/>
				        </spring:url>
				        Theater ${showing.theatre.number} : 
				        <a href="${schedUrl}">
						<fmt:formatDate value="${showing.timestamp}" pattern="yyyy-MM-dd HH:mm:ss" />
						</a>
				        </br>
				    </c:forEach>
				</td>
			</tr>
		</c:forEach>
	</table>
	</div>
</div>