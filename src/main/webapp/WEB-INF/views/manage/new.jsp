<%@ include file="/WEB-INF/views/_taglibs.jspf"%>
<div class="container">
	<div class="row">
		<div class="control-group">
		<form:form action='/manage/${movie.getId()}/processAddShowing' method='POST' modelAttribute="showingForm">
			<div class="controls">
				 Movie Name: <form:input path="movieName" value="${movie.movieName}" readonly="true"/> <br>
				 Theatre ID: <form:input path="theatreId"/> <br>
				 Date and time (24hr):
				 M: <form:input path="month" type="number" min="0" max="12" width="5px"/>
				 D: <form:input path="day" type="number"  min="0" max="31" />
				 Y: <form:input path="year" type="number" min="2017" max="2018" />
				 Hr: <form:input path="hour" type="number" min="10" max="23" />
				 Min: <form:input path="minute" type="number" max="59" min="0" />
				<br>
				 <input type='submit' value="Add new Showing"/>
			</div>
		</form:form>
		</div>
	</div>
</div>