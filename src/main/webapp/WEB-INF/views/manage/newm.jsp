<%@ include file="/WEB-INF/views/_taglibs.jspf"%>
<div class="container">
<div class="row">

<form:form action='/manage/newMovie/process' method='POST' modelAttribute="movieForm">
	Movie name: <form:input path="movieName"/> <br>
	Movie runtime: <form:input path="runtime"/> <br>
    <input type="submit" value="Add Movie"/>
</form:form>
</div>
</div>