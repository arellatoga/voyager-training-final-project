<%@ include file="/WEB-INF/views/_taglibs.jspf"%>
<div class="container">
<div class="row">

Check unavailable seats:
<form:form action='/manage/newTheatre/finalize/process' method='POST' modelAttribute="trueTemplate">
    <form:input path="number" value="${theatreTemplate.number }" readonly="true" />
    <form:input path="x" value="${theatreTemplate.x }" readonly="true" />
    <form:input path="y" value="${theatreTemplate.y }" readonly="true" />
	<table border="1">
	<c:set var="ctr" value="0" scope="page"/>
	<c:forEach begin="0" end="${theatreTemplate.y - 1}" varStatus="y">
		<tr>
		<c:forEach begin="0" end="${theatreTemplate.x - 1}" varStatus="x">
			<td width=20 height=20  align="center">
				<form:checkbox path="noSeats" value="${x.index}-${y.index}"/>
			</td>
		</c:forEach>
		</tr>
	</c:forEach>
	<tr><td colspan="${theatreTemplate.x}" align="center">SCREEN</td></tr>
	</table>
	<input type="submit" value="Create Theatre"/>
</form:form>

</div>
</div>