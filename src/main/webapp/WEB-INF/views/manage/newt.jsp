<%@ include file="/WEB-INF/views/_taglibs.jspf"%>
<div class="container">
<div class="row">

<form:form action='/manage/newTheatre/finalize' method='POST' modelAttribute="theatreTemplate">
	Theatre name/number: <form:input path="number"/> <br>
	Theatre x-size: <form:input path="x"/> <br>
	Theatre y-size: <form:input path="y"/> <br>
    <input type="submit" value="Generate Layout"/>
</form:form>
</div>
</div>