<%@ include file="/WEB-INF/views/_taglibs.jspf"%>
<div class="container">	<div class="row">
    <h1>Theatres:</h1>
	<form action="/manage/newTheatre">
		<input type="submit" value="Add new theatre">
	</form>

	<table>
	<c:forEach var="theatre" items="${theatres }">
	   <tr>
	   <td>
			<spring:url var="theatreUrl" value="manage/theatre/${theatre.number}">
			</spring:url>
			<a href="${theatreUrl}">
			   Theatre ${theatre.number } Layout
			</a>
	   </td>
	   </tr>
	</c:forEach>
	</table>

	<h1><fmt:message key="movies.list.title" /></h1>
	<form action="/manage/newMovie">
		<input type="submit" value="Add new movie">
	</form>
	<table class="table">
		<thead>
			<tr>
				<th>
					<fmt:message key="label.Movie.number"/>
				</th>
				<th>
					<fmt:message key="label.Movie.schedule"/>
				</th>
			</tr>
		</thead>
		<c:forEach var="movie" items="${movies}">
			<tr>
				 <td>${movie.movieName} - ${movie.runtime} minutes
				 <br>
				 <form class="form" action="/manage/${movie.getId()}/new">
				    <input type="submit" value="Add new showing">
				 </form>
				</td>
				<td>
				    <c:forEach var="showing" items="${movie.showings}">
				        <spring:url var="schedUrl" value="manage/{movieId}/{scheduleId}">
				            <spring:param name="movieId" value="${movie.getId()}"/>
				            <spring:param name="scheduleId" value="${showing.id}"/>
				        </spring:url>
				        Theater ${showing.theatre.number}: 
				        <a href="${schedUrl}">
						<fmt:formatDate value="${showing.timestamp}" pattern="yyyy-MM-dd HH:mm:ss" />
						</a>
				        <br>
				    </c:forEach>
				</td>
			</tr>
		</c:forEach>
	</table>
	</div>
</div>