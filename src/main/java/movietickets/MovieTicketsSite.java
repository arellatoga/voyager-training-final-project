package movietickets;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;


@SpringBootApplication
public class MovieTicketsSite extends SpringBootServletInitializer {

    public static void main (String[] args) {
        SpringApplication.run(MovieTicketsSite.class, args);
        //Populator.POPULATE_DATABASE();
    }

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
		return builder.sources(MovieTicketsSite.class);
	}
}
