package movietickets.infrastructure.jpa;

import movietickets.application.exceptions.InvalidMovieException;
import movietickets.domain.model.Movie;
import movietickets.domain.model.Showing;
import movietickets.repository.MovieRepository;
import org.springframework.stereotype.Repository;

import javax.persistence.TypedQuery;
import javax.transaction.Transactional;
import java.util.Collection;
import java.util.List;

@Repository
@Transactional
public class JpaMovieRepository extends JpaGenericRepository<Movie, Long> implements MovieRepository {

    public List<Movie> getAllMovies() {
        TypedQuery<Movie> query = getEntityManager().createQuery(
                "SELECT a FROM Movie a", Movie.class);
        return query.getResultList();
    }

    @Override
    protected Class<Movie> getEntityClass() {
        return Movie.class;
    }

    @Override
    public List<Showing> findShowingsBy(String movieName){
        return null;
    }

	@Override
	public void addMovie(Movie movie) {
		this.save(movie);
	}

	@Override
	public Movie getMovieById(Long id) {
		TypedQuery<Movie> query = getEntityManager().createQuery(
				"SELECT a FROM Movie a WHERE id = :id", Movie.class);
		query.setParameter("id", id);
		return query.getSingleResult();
	}

	@Override
	public Movie getMovieByName(String name) throws InvalidMovieException {
		try {
			TypedQuery<Movie> query = getEntityManager().createQuery(
					"SELECT a FROM Movie a WHERE a.movieName = :name", Movie.class);
			query.setParameter("name", name);
			return query.getSingleResult();
		}
		catch (Exception e) {
			throw new InvalidMovieException("No such movie found");
		}
	}
}
