package movietickets.infrastructure.jpa;

import java.util.List;

import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import movietickets.application.exceptions.InvalidSeatException;
import movietickets.domain.model.Seat;
import movietickets.repository.SeatRepository;

@Repository
@Transactional
public class JpaSeatRepository extends JpaGenericRepository<Seat, Long> implements SeatRepository {
    @Override
    protected Class<Seat> getEntityClass() {
        return Seat.class;
    }

    @Override
    public Seat getSeatWithId(String id) {
        return null;
    }

	@Override
	public void addSeat(Seat seat) {
		this.save(seat);
	}

	@Override
	public List<Seat> getSeatsFromTheatreWithNumber(String number) throws InvalidSeatException {
		TypedQuery<Seat> query = getEntityManager().createQuery(
				"SELECT a FROM Seat a JOIN a.theatre t WHERE t.number = :number", Seat.class);
		query.setParameter("number", number);
		return query.getResultList();
	}

	@Override
	public List<Seat> getSeatFromShowingWithId(Long id) throws Exception {
		TypedQuery<Seat> query = getEntityManager().createQuery(
				"SELECT s FROM Seat s JOIN s.theater t JOIN t.showings m WHERE m.id = :id", Seat.class);
		query.setParameter("id", id);
		return query.getResultList();
	}
}
