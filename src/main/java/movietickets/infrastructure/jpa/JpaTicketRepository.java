package movietickets.infrastructure.jpa;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import movietickets.domain.model.Showing;
import movietickets.domain.model.Ticket;
import movietickets.repository.TicketRepository;

@Repository
@Transactional
public class JpaTicketRepository extends JpaGenericRepository<Ticket, Long> implements TicketRepository {

    @Override
    public Ticket getTicketForShowing(Showing showing, int x, int y) {
        return null;
    }

    @Override
    protected Class<Ticket> getEntityClass() {
        return Ticket.class;
    }

	@Override
	public void addTicket(Ticket ticket) {
		this.save(ticket);
	}
}
