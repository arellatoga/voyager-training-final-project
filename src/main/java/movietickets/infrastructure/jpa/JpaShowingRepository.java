package movietickets.infrastructure.jpa;

import java.util.List;

import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import movietickets.domain.model.Showing;
import movietickets.repository.ShowingRepository;

@Repository
@Transactional
public class JpaShowingRepository extends JpaGenericRepository<Showing, Long> implements ShowingRepository{
    @Override
    protected Class<Showing> getEntityClass() {
        return Showing.class;
    }

    @Override
    public Showing getShowingWithId(Long id) {
		TypedQuery<Showing> query = getEntityManager().createQuery(
				"SELECT a FROM Showing a WHERE id = :id", Showing.class);
		query.setParameter("id", id);
		return query.getSingleResult();
    }

	@Override
	public void addShowing(Showing showing) {
		this.save(showing);
	}

	@Override
	public List<Showing> getAllShowings() {
		TypedQuery<Showing> query = getEntityManager().createQuery(
				"SELECT a from Showing a", Showing.class);
		return query.getResultList();
	}
}
