package movietickets.infrastructure.jpa;

import java.util.List;

import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import movietickets.application.exceptions.InvalidTheatreException;
import movietickets.domain.model.Theatre;
import movietickets.repository.TheatreRepository;

@Repository
@Transactional
public class JpaTheatreRepository extends JpaGenericRepository<Theatre, Long> implements TheatreRepository {
    @Override
    protected Class<Theatre> getEntityClass() {
        return Theatre.class;
    }

    public List<Theatre> getAllTheatres() {
        TypedQuery<Theatre> query = getEntityManager().createQuery(
                "SELECT a FROM Theatre a", Theatre.class);
        return query.getResultList();
    }

    @Override
    public Theatre getTheatreWithId(int id) {
        return null;
    }

	@Override
	public void addTheatre(Theatre theatre) {
		this.save(theatre);
	}
	
	@Override
    public Theatre getTheaterWithNumber(String name) throws InvalidTheatreException {
		try {
			TypedQuery<Theatre>query = getEntityManager().createQuery(
					"SELECT a FROM Theatre a WHERE a.number = :number", Theatre.class);
			query.setParameter("number", name);
			return query.getSingleResult();
		}
		catch (Exception e) {
			throw new InvalidTheatreException("Theater with number " + name + " not found");
		}
	}
}
