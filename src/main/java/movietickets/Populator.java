package movietickets;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import movietickets.domain.model.*;
import movietickets.infrastructure.jpa.*;
import movietickets.repository.*;

@Component
@Transactional
public class Populator {
	
	@Autowired
	MovieRepository movieRepository;
	@Autowired
	TicketRepository ticketRepository;
	@Autowired
	SeatRepository seatRepository;
	@Autowired
	TheatreRepository theatreRepository;
	@Autowired
	ShowingRepository showingRepository;
	
	@Autowired
	public Populator (MovieRepository mrep,
					TicketRepository trep,
					SeatRepository srep,
					TheatreRepository threp,
					ShowingRepository shrep) {
		this.movieRepository = mrep;
		this.ticketRepository = trep;
		this.seatRepository = srep;
		this.theatreRepository = threp;
		this.showingRepository = shrep;
	}

	public void POPULATE_DATABASE() {
		Movie movie1 = new Movie("The Dark Knight", 60);
		Movie movie2 = new Movie("It", 60);
		Movie movie3 = new Movie("Jacob's Ladder", 60);
		Movie movie4 = new Movie("Pulp Fiction", 60);

		Theatre theatre1 = new Theatre("1", 10, 5);
		Theatre theatre2 = new Theatre("X", 1, 1);
		
		Seat seat1 = new Seat(theatre1, 0, 0, true, true);
		Seat seat2 = new Seat(theatre1, 0, 1, true, true);
		Seat seat3 = new Seat(theatre1, 1, 0, true);
		Seat seat4 = new Seat(theatre1, 1, 1, true);
		
		Ticket ticket1 = new Ticket(seat1);
		Ticket ticket2 = new Ticket(seat2);
		
		theatre1.addSeat(seat1);
		theatre1.addSeat(seat2);
		theatre1.addSeat(seat3);
		theatre1.addSeat(seat4);
		
		Date date1 = newDate(new GregorianCalendar(2017, Calendar.OCTOBER, 10), 10, 30);
		Date date2 = newDate(new GregorianCalendar(2017, Calendar.OCTOBER, 10), 12, 00);
		Date date3 = newDate(new GregorianCalendar(2017, Calendar.OCTOBER, 10), 14, 30);
		
		Showing showing1 = new Showing(theatre1, date1, movie1.getMovieName());
		Showing showing2 = new Showing(theatre1, date2, movie1.getMovieName());
		Showing showing3 = new Showing(theatre1, date3, movie1.getMovieName());
		Showing showing4 = new Showing(theatre2, date1, movie1.getMovieName());
		
		movie1.addShowing(showing4);
		theatre2.addShowing(showing4);
		
		movie1.addShowing(showing1);
		theatre1.addShowing(showing1);
		movie1.addShowing(showing2);
		theatre1.addShowing(showing2);
		movie1.addShowing(showing3);
		theatre1.addShowing(showing3);
		showing1.reserveTicket(ticket1);
		showing1.reserveTicket(ticket2);
		
		movieRepository.addMovie(movie1);
		movieRepository.addMovie(movie2);
		movieRepository.addMovie(movie3);
		movieRepository.addMovie(movie4);
		theatreRepository.addTheatre(theatre1);
		theatreRepository.addTheatre(theatre2);
		seatRepository.addSeat(seat1);
		seatRepository.addSeat(seat2);
		seatRepository.addSeat(seat3);
		seatRepository.addSeat(seat4);
		showingRepository.addShowing(showing1);
		showingRepository.addShowing(showing2);
		showingRepository.addShowing(showing3);
		showingRepository.addShowing(showing4);
		ticketRepository.addTicket(ticket1);
		ticketRepository.addTicket(ticket2);
	}

	private Date newDate(GregorianCalendar gc, int hour, int minute) {
		gc.set(Calendar.HOUR_OF_DAY, hour);
		gc.set(Calendar.MINUTE, minute);
		return gc.getTime();
	}
}
