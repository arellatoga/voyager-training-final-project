package movietickets.repository;

import java.util.List;

import movietickets.domain.model.Showing;
import movietickets.infrastructure.jpa.GenericRepository;

public interface ShowingRepository extends GenericRepository<Showing, Long>{
	List<Showing> getAllShowings();
    Showing getShowingWithId(Long id);
    void addShowing(Showing showing);
}
