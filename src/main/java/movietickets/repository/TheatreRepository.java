package movietickets.repository;

import java.util.List;

import movietickets.application.exceptions.InvalidTheatreException;
import movietickets.domain.model.Theatre;
import movietickets.infrastructure.jpa.GenericRepository;

public interface TheatreRepository extends GenericRepository<Theatre, Long> {
    Theatre getTheatreWithId(int id);
    void addTheatre(Theatre theatre); 
    List<Theatre> getAllTheatres();
    Theatre getTheaterWithNumber(String name) throws InvalidTheatreException;
}
