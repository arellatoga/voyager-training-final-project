package movietickets.repository;

import movietickets.domain.model.Showing;
import movietickets.domain.model.Ticket;
import movietickets.infrastructure.jpa.GenericRepository;

public interface TicketRepository extends GenericRepository<Ticket, Long> {
    Ticket getTicketForShowing(Showing showing, int x, int y);
    void addTicket(Ticket ticket);
}
