package movietickets.repository;

import movietickets.application.exceptions.InvalidMovieException;
import movietickets.domain.model.Movie;
import movietickets.domain.model.Showing;
import movietickets.infrastructure.jpa.GenericRepository;

import java.util.Collection;
import java.util.List;

public interface MovieRepository extends GenericRepository<Movie, Long> {
    List<Movie> getAllMovies();
    List<Showing> findShowingsBy(String movieName);
    Movie getMovieById(Long id);
    void addMovie(Movie movie);
    Movie getMovieByName(String name) throws InvalidMovieException;
}
