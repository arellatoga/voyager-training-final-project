package movietickets.repository;

import java.util.List;

import movietickets.domain.model.Seat;
import movietickets.infrastructure.jpa.GenericRepository;

public interface SeatRepository extends GenericRepository<Seat, Long> {
    Seat getSeatWithId(String id);
    void addSeat(Seat seat);
    List<Seat> getSeatsFromTheatreWithNumber(String number) throws Exception;
    List<Seat> getSeatFromShowingWithId(Long id) throws Exception;
}
