package movietickets.application;

import movietickets.domain.model.Showing;

import movietickets.domain.model.Theatre;
import movietickets.application.exceptions.ExistingMovieException;
import movietickets.application.exceptions.ExistingTheatreException;
import movietickets.application.exceptions.InvalidMovieException;
import movietickets.application.exceptions.InvalidSeatException;
import movietickets.application.exceptions.InvalidShowingException;
import movietickets.application.exceptions.InvalidTheatreException;
import movietickets.application.exceptions.InvalidTicketException;
import movietickets.domain.model.Movie;
import movietickets.domain.model.Seat;
import movietickets.domain.model.Ticket;
import movietickets.domain.model.form.MovieForm;
import movietickets.domain.model.form.ShowingForm;
import movietickets.domain.model.form.TheatreTemplateForm;
import movietickets.domain.model.form.TheatreTemplateWithSeatsForm;
import movietickets.repository.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import java.lang.*;
import javax.transaction.Transactional;

@Service
@Transactional
public class MovieReservationServiceImpl implements MovieReservationService {

	MovieRepository movieRepository;
	TicketRepository ticketRepository;
	TheatreRepository theatreRepository;
	SeatRepository seatRepository;
	ShowingRepository showingRepository;

    @Autowired
    public MovieReservationServiceImpl(
    		MovieRepository movieRepository,
    		TicketRepository ticketRepository,
    		TheatreRepository theatreRepository,
    		SeatRepository seatRepository,
    		ShowingRepository showingRepository) {
    		this.movieRepository = movieRepository;
    		this.seatRepository = seatRepository;
    		this.theatreRepository = theatreRepository;
    		this.showingRepository = showingRepository;
    		this.ticketRepository = ticketRepository;  	
    }
    
    @Override
    public List<Movie> findAllMovies() {
    		return movieRepository.getAllMovies();
    }

    @Override
    public Ticket buyTicketForShowing(Showing showing, Seat seat) throws Exception {
		if (isSeatInShowingOccupied(seat, showing)) {
			throw new InvalidTicketException("Seat is occupied");
		}
		List<Seat> seats = seatRepository.getSeatsFromTheatreWithNumber(showing.getTheatre().getNumber());
		for (Seat eseat : seats) {
			if (eseat.getX() == seat.getX() &&
				eseat.getX() == seat.getY()) {
				if (!eseat.getAvailable()) {
					throw new InvalidSeatException("Seat is not available for reservations");
				}
			}
		}
		Theatre theatre = showing.getTheatre();
		theatre.addSeat(seat);
		seat.setTheatre(theatre);
		seat.setAvailable(true);
		seat.setOccupied(true);
		Ticket ticket = new Ticket(seat);
		showing.reserveTicket(ticket);
		return ticket;
    }

	@Override
	public Showing findShowingWithId(Long id) {
		return showingRepository.getShowingWithId(id);
	}

	@Override
	public Movie findMovieWithId(Long id) {
		return movieRepository.getMovieById(id);
	}

	@Override
	public Theatre getTheatreOfShowingWithId(Long id) {
		return findShowingWithId(id).getTheatre();
	}
	
	@Override
	public Ticket getTicketForSeatAndShowing(Seat seat, Showing showing) throws Exception {
		List<Ticket> tickets = showing.getAllTickets();
		for (Ticket ticket : tickets) {
			Seat ticketseat = ticket.getSeat();
			if (ticketseat.getTheatre() == null) {
				throw new InvalidTheatreException("Seat is not assigned to a theatre!");
			}
			if (ticketseat.getX() == seat.getX() &&
				ticketseat.getY() == seat.getY()) {
				return ticket;
			}
		}
		System.out.println("Did not find ticket");
		throw new InvalidTicketException("Ticket was not found for given seat");
	}

	@Override
	public List<Seat> getAllSeatsInTheatreWithNumber(String number) throws Exception {
		Theatre theatre = theatreRepository.getTheaterWithNumber(number);
		List<Seat> tSeats = theatre.getAllSeats();
		ArrayList<Seat> seats = new ArrayList();
		
		for (int i = 0; i < theatre.getMaxY(); i++) {
			for (int j = 0; j < theatre.getMaxX(); j++) {
				Seat seat = new Seat(theatre, j, i, true, false);
				for (Seat tSeat : tSeats) {
					if (tSeat.getX() == j && tSeat.getY() == i) {
						if (!tSeat.getAvailable()) {
							seat.setAvailable(false);
							break;
						}
					}
				}
				seats.add(seat);
			}
		}
		return seats;
	}

	@Override
	public List<Seat> getAllSeatsInShowingWithId(Long id) throws Exception {
		Showing showing = findShowingWithId(id);
		Theatre theater = showing.getTheatre();
		List<Ticket> tickets = showing.getAllTickets();
		List<Seat> tSeats = theater.getAllSeats();
		ArrayList<Seat> seats = new ArrayList();
		for (int i = 0; i < theater.getMaxY(); i++) {
			for (int j = 0; j < theater.getMaxX(); j++) {
				Seat seat = new Seat(theater, j, i, true, false);
				for (Seat tSeat : tSeats) {
					if (tSeat.getX() == j && tSeat.getY() == i) {
						if (!tSeat.getAvailable()) {
							seat.setAvailable(false);
							break;
						}
					}
				}
				if (seat.getAvailable()) {
					for (Ticket ticket : tickets) {
						if (ticket.getSeat().equals(seat)) {
							seat = ticket.getSeat();
							tickets.remove(ticket);
							break;
						}
					}
				}
				seats.add(seat);
			}
		}
		return seats;
		//return seatRepository.getSeatFromShowingWithId(id);
	}

	@Override
	public boolean isSeatInShowingOccupied(Seat seat, Showing showing) {
		List<Ticket> tickets = showing.getAllTickets();
		for (Ticket ticket : tickets) {
			if (ticket.hasSeat(seat)) {
				return true;
			}
		}
		return false;
	}

	@Override
	public List<Theatre> findAllTheatres() {
		return theatreRepository.getAllTheatres();
	}

	@Override
	public Movie findMovieWithName(String movieName) throws InvalidMovieException {
		Movie movie = movieRepository.getMovieByName(movieName);
		if (movie == null) {
			System.out.println("Unable to find movie" + movieName);
			throw new InvalidMovieException("Movie with name " + movieName + " was not found");
		}
		return movie;
	}

	@Override
	public List<Showing> createShowing(ShowingForm showForm) throws InvalidShowingException, InvalidMovieException, InvalidTheatreException {
		try {
			Movie movie = this.findMovieWithName(showForm.getMovieName());	
			Theatre theatre = theatreRepository.getTheaterWithNumber(showForm.getTheatreId());
			List<Showing> showings = showingRepository.getAllShowings();
			List<Date> createdShowingStamps = new ArrayList();

			for (int i = 0; i < 7; i++) {
				Date newShowStamp = newDate(new GregorianCalendar(showForm.getYear(), showForm.getMonth() - 1, showForm.getDay() + i), showForm.getHour(), showForm.getMinute());
				
				if (showForm.getHour() < 10 && showForm.getMinute() < 30) {
					throw new InvalidShowingException("Planned showing is too early!");
				}
				
				if (showForm.getHour() > 23 || showForm.getMinute() > 59) {
					throw new InvalidShowingException("Invalid times for the showing!");
				}

				// Check if all showings of movie overlap
				for (Showing showing : showings) {
					if (!showing.getTheatre().getNumber().equals(showForm.getTheatreId())) {
						continue;
					}
					Date timeStamp = showing.getTimestamp();
					Calendar cal = Calendar.getInstance();
					cal.setTime(newShowStamp);
					int nHours = cal.get(Calendar.HOUR_OF_DAY);
					int nMin = cal.get(Calendar.MINUTE);
					int nTime = (nHours * 60) + nMin;
					int nday = cal.get(Calendar.DAY_OF_YEAR);
					int nyear = cal.get(Calendar.YEAR);
					
					cal.setTime(timeStamp);
					int oHours = cal.get(Calendar.HOUR_OF_DAY);
					int oMin = cal.get(Calendar.MINUTE);
					int oTime = (oHours * 60) + oMin;
					int oday = cal.get(Calendar.DAY_OF_YEAR);
					int oyear = cal.get(Calendar.YEAR);
					
					Movie oMovie = movieRepository.getMovieByName(showing.getMovieName());

					if (nday != oday || nyear != oyear) {
						continue;
					}
					// AABB Algorithm
					// Throw exception if there's an existing overlap
					if (Math.abs(nTime - oTime) * 2 < ((movie.getRuntime() + 10) + (oMovie.getRuntime() + 10))) {
						throw new InvalidShowingException("Planned showing for " + showForm.getMovieName() +
								" overlaps with existing showing " + showing.getMovieName() + "!");
					}
				}
				createdShowingStamps.add(newShowStamp);
			}
			
			List<Showing> createdShowings = new ArrayList();
			for (Date stamp : createdShowingStamps) {
				Showing newShowing = new Showing(theatre, stamp, movie.getMovieName());
				movie.addShowing(newShowing);
				theatre.addShowing(newShowing);
				createdShowings.add(newShowing);
			}
			
			return createdShowings;
		}
		catch (Exception e) {
			throw e;
		}
	}

	@Override
	public Theatre findTheatreWithNumber(String number) throws InvalidTheatreException {
		Theatre theatre = theatreRepository.getTheaterWithNumber(number);
		if (theatre == null) {
			System.out.println("Did not find theatre with number: " + number);
			throw new InvalidTheatreException("Theater with id " + number + " was not found!");
		}
		return theatre;
	}

	private Date newDate(GregorianCalendar gc, int hour, int minute) {
		gc.set(Calendar.HOUR_OF_DAY, hour);
		gc.set(Calendar.MINUTE, minute);
		return gc.getTime();
	}

	@Override
	public Theatre createTheatre(TheatreTemplateWithSeatsForm template) throws ExistingTheatreException {
		Theatre test;
		try {
			test = theatreRepository.getTheaterWithNumber(template.getNumber());
			if (test != null) {
				throw new ExistingTheatreException("Theatre already exists");
			}
		}
		catch (InvalidTheatreException e) {
			// do nothing, if it's invalid, then it's good
		}
		Theatre theatre = new Theatre(template.getNumber(), template.getX(), template.getY());
		theatreRepository.addTheatre(theatre);
		for (String string : template.getNoSeats()) {
			String[] xy = string.split("-");
			Seat seat = new Seat(theatre, Integer.parseInt(xy[0]), Integer.parseInt(xy[1]), false);
			theatre.addSeat(seat);
		}
		return theatre;
	}

	@Override
	public Movie createMovie(MovieForm form) throws Exception {
		Movie test;
		try {
			test = movieRepository.getMovieByName(form.getMovieName());
			if (test != null) {
				throw new ExistingMovieException("Movie already exists");
			}
		}
		catch (InvalidMovieException e) {
			// do nothing
		}
		Movie movie = new Movie(form.getMovieName(), form.getRuntime());
		movieRepository.addMovie(movie);
		return movie;
	}
}
