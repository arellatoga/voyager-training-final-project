package movietickets.application;

import java.util.List;

import movietickets.domain.model.Movie;
import movietickets.domain.model.Seat;
import movietickets.domain.model.Showing;
import movietickets.domain.model.Theatre;
import movietickets.domain.model.Ticket;
import movietickets.domain.model.form.MovieForm;
import movietickets.domain.model.form.ShowingForm;
import movietickets.domain.model.form.TheatreTemplateForm;
import movietickets.domain.model.form.TheatreTemplateWithSeatsForm;

public interface MovieReservationService {
    Ticket buyTicketForShowing(Showing showing, Seat seat) throws Exception;
    List<Movie> findAllMovies();
    Showing findShowingWithId(Long id);
    Theatre getTheatreOfShowingWithId(Long id);
    Movie findMovieWithId(Long id);
    List<Seat> getAllSeatsInTheatreWithNumber(String number) throws Exception;
    List<Seat> getAllSeatsInShowingWithId(Long id) throws Exception;
    boolean isSeatInShowingOccupied(Seat seat, Showing showing);
	Ticket getTicketForSeatAndShowing(Seat seat, Showing showing) throws Exception;
	List<Theatre> findAllTheatres();
	Movie findMovieWithName(String movieName) throws Exception;
	List<Showing> createShowing(ShowingForm showing) throws Exception;
	Theatre findTheatreWithNumber(String number) throws Exception;
	Theatre createTheatre(TheatreTemplateWithSeatsForm template) throws Exception;
	Movie createMovie(MovieForm form) throws Exception;
}
