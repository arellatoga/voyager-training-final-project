package movietickets.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
public class MovieTicketsSecurityConfig extends WebSecurityConfigurerAdapter {
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http
			.formLogin()
			.loginPage("/login")
				.permitAll()
				.and()
			.exceptionHandling()
				.accessDeniedPage("/denied")
				.and()
			.authorizeRequests()
				.antMatchers(HttpMethod.GET, "/manage").hasRole("ADMIN")
				.antMatchers(HttpMethod.GET, "/manage/**").hasRole("ADMIN")
				.antMatchers(HttpMethod.GET, "/movies/*/*").hasRole("REPRESENTATIVE")
				.antMatchers("/report").hasRole("ADMIN")
				.and()
			.logout()
				.permitAll()
				.logoutSuccessUrl("/");
	}
	
	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
		auth
			.inMemoryAuthentication()
			.withUser("bigguy").password("4you").roles("REPRESENTATIVE").and()
			.withUser("rep").password("rep").roles("REPRESENTATIVE").and()
			.withUser("admin").password("admin").roles("ADMIN").and()
			.withUser("badmin").password("badmin").roles("ADMIN");
	}
}
