package movietickets.web;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import movietickets.application.MovieReservationService;
import movietickets.domain.model.Showing;
import movietickets.domain.model.Theatre;
import movietickets.domain.model.form.MovieForm;
import movietickets.domain.model.form.ShowingForm;
import movietickets.domain.model.form.TheatreTemplateForm;
import movietickets.domain.model.form.TheatreTemplateWithSeatsForm;

@Controller
@RequestMapping("/" + ManagementController.PATH)
public class ManagementController {
	public static final String PATH = "manage";
	
	private MovieReservationService service;
	
	@Autowired
	public ManagementController(MovieReservationService service) {
		this.service = service;
	}
	
	@RequestMapping(method = RequestMethod.GET)
	public String index(Model model) {
		model.addAttribute("theatres", service.findAllTheatres());
		model.addAttribute("movies", service.findAllMovies());
		return PATH + "/list";
	}
	
	@RequestMapping("/{movieId}/new")
	public String newShowing(@PathVariable("movieId") Long movieId, Model model) {
		model.addAttribute("movie", service.findMovieWithId(movieId));
		model.addAttribute("showingForm", new ShowingForm());
		return PATH + "/new";
	}
	
	@PostMapping("/{movieId}/processAddShowing")
	public String processAddShow(@ModelAttribute("showingForm") ShowingForm form, BindingResult result, @PathVariable("movieId") Long movieId, Model model) {
		try {
			service.createShowing(form);
			return "redirect:new";
		}
		catch (Exception e) {
			model.addAttribute("exceptionMessage", e.getMessage());
			return PATH + "/error";
		}
	}
	
	@RequestMapping("/theatre")
	public String redirectTheatreBlank() {
		return "redirect:/manage";
	}
	
	@RequestMapping("/theatre/{theatreNo}")
	public String showTheatre(@PathVariable("theatreNo") String theatreNo, Model model) {
		try {
			Theatre theatre = service.findTheatreWithNumber(theatreNo);
			model.addAttribute("theatre", theatre);
			model.addAttribute("seats", service.getAllSeatsInTheatreWithNumber(theatreNo));
			return PATH + "/showt";
		}
		catch (Exception e) {
			model.addAttribute("exceptionMessage", e.getMessage());
			return "/error";
		}
	}
	
	@RequestMapping(method = RequestMethod.GET, path = "/{movieId}/{showingId}")
	public String show(@PathVariable("movieId") Long movieId, @PathVariable("showingId") Long showingId, Model model) {
		try {
			Theatre theatre = service.getTheatreOfShowingWithId(showingId);
			model.addAttribute("showing", service.findShowingWithId(showingId));
			model.addAttribute("theatre", theatre);
			model.addAttribute("movie", service.findMovieWithId(movieId));
			model.addAttribute("seats", service.getAllSeatsInShowingWithId(showingId));
			return PATH + "/show";
		}
		catch (Exception e) {
			model.addAttribute("exceptionMessage", "Seats were not found");
			return "/error";
		}
	}
	
	@RequestMapping("/newTheatre")
	public String newTheatre(Model model) {
		model.addAttribute("theatreTemplate", new TheatreTemplateForm());
		return PATH + "/newt";
	}
	
	@PostMapping("/newTheatre/finalize")
	public String newTheatreFinalize(@Valid @ModelAttribute("theatreTemplate") TheatreTemplateForm theatreTemplate, BindingResult bindingResult, Model model) {
		if (bindingResult.hasErrors()) {
			model.addAttribute("exceptionMessage", "Binding result has errors!");
			return "/error";
		}
		if (theatreTemplate.getNumber().equals("")) {
			model.addAttribute("exceptionMessage", "Name/number can not be empty!");
			return "/error";
		}
		if (theatreTemplate.getX() < 1 || theatreTemplate.getY() < 1) {
			model.addAttribute("exceptionMessage", "Invalid seat count: " + theatreTemplate.getX() + " " + theatreTemplate.getY());
			return "/error";
		}
		model.addAttribute("theatreTemplate", theatreTemplate);
		model.addAttribute("trueTemplate", new TheatreTemplateWithSeatsForm());
		return PATH + "/newtt";
	}
	
	@PostMapping("/newTheatre/finalize/process")
	public String newTheatreFinalizeProcess(@Valid @ModelAttribute("trueTemplate") TheatreTemplateWithSeatsForm theatreTemplate, BindingResult bindingResult, Model model) {
		if (bindingResult.hasErrors()) {
			model.addAttribute("exceptionMessage", "Binding result has errors!");
			return "/error";
		}
		try {
			service.createTheatre(theatreTemplate);
			return "redirect:/manage/newTheatre";
		}
		catch (Exception e) {
			model.addAttribute("exceptionMessage", e.getMessage());
			return "/error";
		}
	}
	
	@RequestMapping("/newMovie")
	public String newMovie(Model model) {
		model.addAttribute("movieForm", new MovieForm());
		return PATH + "/newm";
	}
	
	@PostMapping("/newMovie/process")
	public String newMovieProcess(@Valid @ModelAttribute("movieForm") MovieForm form, BindingResult result, Model model) {
		if (result.hasErrors()) {
			model.addAttribute("exceptionMessage", "Binding result has errors");
		}
		try {
			service.createMovie(form);
			return "redirect:/manage/newMovie";
		}
		catch (Exception e) {
			model.addAttribute("exceptionMessage", e.getMessage());
			return "/error";
		}
	}
	
	/*
	@InitBinder
	public void initBinder(WebDataBinder binder) {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		sdf.setLenient(true);
		binder.registerCustomEditor(Date.class, new CustomDateEditor(sdf, true));
	}
	*/
}
