package movietickets.web;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.view.RedirectView;

import movietickets.Populator;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

import org.springframework.beans.factory.annotation.Autowired;

@Controller
public class IndexController {
	
	@Autowired
	public IndexController(Populator populator) {
		// Populates database: 
		// Uncomment after
		populator.POPULATE_DATABASE();
	}

    @RequestMapping("/")
    public String index(Model model) {
        System.out.println("Entered index");
        return "index";
    }
    
    @GetMapping("/logout")
    public RedirectView logout() {
    		return new RedirectView("/");
    }
}
