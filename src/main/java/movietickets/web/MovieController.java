package movietickets.web;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.view.RedirectView;

import movietickets.application.MovieReservationService;
import movietickets.application.exceptions.InvalidSeatException;
import movietickets.application.exceptions.InvalidTicketException;
import movietickets.domain.model.*;

@Controller
@RequestMapping("/" + MovieController.PATH)
public class MovieController {
	public static final String PATH = "movies";
	
	private MovieReservationService service;
	
	@Autowired
	public MovieController(MovieReservationService service) {
		this.service = service;
	}
	
	@RequestMapping(method = RequestMethod.GET)
	public String index(Model model) {
		model.addAttribute("movies", service.findAllMovies());
		return PATH + "/list";
	}
	
	@GetMapping("/{movieNumber}")
	public RedirectView redirect() {
		return new RedirectView("/movies");
	}
	
	@RequestMapping(method = RequestMethod.GET, path = "/{movieId}/{showingId}")
	public String show(@PathVariable("movieId") Long movieId, @PathVariable("showingId") Long showingId, Model model) {
		try {
			Theatre theatre = service.getTheatreOfShowingWithId(showingId);
			model.addAttribute("showing", service.findShowingWithId(showingId));
			model.addAttribute("theatre", theatre);
			model.addAttribute("movie", service.findMovieWithId(movieId));
			model.addAttribute("seats", service.getAllSeatsInShowingWithId(showingId));
			model.addAttribute("seat", new Seat(theatre, 0, 0));
			return PATH + "/buy";
		}
		catch (Exception e) {
			model.addAttribute("exceptionMessage", "Seats were not found");
			return "/error";
		}
	}
	
	@PostMapping(path = "/{movieId}/{showingId}/ticket")
	public String getTicket(@PathVariable("movieId") Long movieId, @PathVariable("showingId") Long showingId, Model model, @Valid Seat seat, BindingResult result) {
		try {
			Showing showing = service.findShowingWithId(showingId);
			model.addAttribute("showing", showing);
			model.addAttribute("theatre", showing.getTheatre());
			model.addAttribute("movie", service.findMovieWithId(movieId));
			model.addAttribute("ticket", service.getTicketForSeatAndShowing(seat, showing));
			return PATH + "/ticket";
		}
		catch (Exception e) {
			model.addAttribute("exceptionMessage", e.getMessage());
			return "/error";
		}
	}
	
	@PostMapping(path = "/{movieId}/{showingId}/buy")
	public String processBuy(@PathVariable("movieId") Long movieId, @PathVariable("showingId") Long showingId, Model model, @Valid Seat seat, BindingResult result) {
		try {
			Showing showing = service.findShowingWithId(showingId);
			
			try {
				Ticket ticket = service.getTicketForSeatAndShowing(seat, showing);
				if (ticket != null) {
					throw new InvalidSeatException("Seat is already taken");
				}
			}
			catch (InvalidTicketException e) {
				// do nothing
			}

			if (showing == null) {
				throw new Exception();
			}
			service.buyTicketForShowing(showing, seat);

			return "redirect:";
		}
		catch (Exception e) {
			model.addAttribute("exceptionMessage", e.getMessage());
			return "/error";
		}
	}
}
