package movietickets.domain.model;

import javax.persistence.*;

import static javax.persistence.GenerationType.AUTO;

@Entity
@Table(name = "T_TICKET")
public class Ticket {
    @Id
    @GeneratedValue(strategy = AUTO)
    private Long id;

    @OneToOne(cascade = {CascadeType.ALL})
    @JoinColumn(name = "seat_id")
    private Seat seat;
    
    @ManyToOne(cascade = {CascadeType.ALL})
    @JoinColumn(name = "showing_id")
    private Showing showing;

    public Ticket() {
        // no-args constructor
    }

    public Ticket(Seat seat) {
        this.seat = seat;
    }
    
    public boolean hasSeat(Seat seat) {
    		if (this.seat.equals(seat)) {
    			return true;
    		}
    		return false;
    }
    
    public Seat getSeat() {
		return seat;
    }
    
    public void setSeat(Seat seat) {
    		this.seat = seat;
    }
    
    public Long getId() {
		return id;
    }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((seat == null) ? 0 : seat.hashCode());
		result = prime * result + ((showing == null) ? 0 : showing.hashCode());
		return result;
	}
}
