package movietickets.domain.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Entity
@Table(name = "T_THEATRE")
public class Theatre
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    
    @Column(name = "number")
    private String number;

    @OneToMany(cascade = {CascadeType.ALL})
    @JoinColumn(name = "theatre_id")
    private Collection<Showing> showings;
    
    @OneToMany(cascade = {CascadeType.ALL})
    @JoinColumn(name = "theatre_id")
    private Collection<Seat> seats;
    
    @Column(name = "max_x")
    private int max_x;

    @Column(name = "max_y")
    private int max_y;

    public Theatre() {
        // Empty constructor
    }
    
    public String getNumber () {
    		return number;
    }

    public Theatre(String number, int max_x, int max_y) {
		this.number = number;
		this.max_x = max_x;
		this.max_y = max_y;
		seats = new ArrayList<>();
		showings = new ArrayList<>();
    }

    public Collection<Showing> addShowing(Showing showing) {
        if (showings == null) {
            showings = new ArrayList<Showing>();
        }
        showings.add(showing);
        return new ArrayList<>(showings);
    }

    @Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + max_x;
		result = prime * result + max_y;
		result = prime * result + ((number == null) ? 0 : number.hashCode());
		result = prime * result + ((seats == null) ? 0 : seats.hashCode());
		result = prime * result + ((showings == null) ? 0 : showings.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Theatre other = (Theatre) obj;
		if (max_x != other.max_x)
			return false;
		if (max_y != other.max_y)
			return false;
		if (number == null) {
			if (other.number != null)
				return false;
		} else if (!number.equals(other.number))
			return false;
		if (seats == null) {
			if (other.seats != null)
				return false;
		} else if (!seats.equals(other.seats))
			return false;
		if (showings == null) {
			if (other.showings != null)
				return false;
		} else if (!showings.equals(other.showings))
			return false;
		return true;
	}

	public String toString() {
		return number;
    }
    
    public List<Seat> getAllSeats() {
    		return new ArrayList(seats);
    }
    
    public int getMaxSeats() {
    		return max_x * max_y;
    }
    
    public int getMaxX() {
    		return max_x;
    }
    
    public int getMaxY() {
    		return max_y;
    }
    
    public void addSeat(Seat seat) {
    		seats.add(seat);
    }
}
