package movietickets.domain.model;

import javax.persistence.*;

import org.springframework.format.annotation.DateTimeFormat;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "T_SHOWING")
public class Showing {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "time_stamp")
    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm")
    private Date timestamp;

    @Column(name = "showing_name")
    private String moviename;
    
    @OneToMany(cascade = {CascadeType.ALL})
    @JoinColumn(name = "showing_id")
    private Collection<Ticket> tickets;
    
    @ManyToOne(cascade = {CascadeType.ALL})
    @JoinColumn(name = "theatre_id")
    private Theatre theatre;

    public Showing() {
        // no-args constructor
    }

    public Showing(Theatre theatre, Date timestamp, String moviename) {
    		this.theatre = theatre;
        this.timestamp = timestamp;
        this.moviename = moviename;
        this.tickets = new ArrayList<>();
    }
    
    public Long getId() {
		return id;
    }
    
    public void reserveTicket(Ticket ticket) {
    		tickets.add(ticket);
    }
    
    public String getMovieName() {
    		return moviename;
    }
    
    public void setMovieName(String moviename) {
		this.moviename = moviename;
    }
    
    public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
    }
    
    public Date getTimestamp() {
    		return timestamp;
    }
    
    public Theatre getTheatre() {
		return theatre;
    }
    
    public void setTheatre(Theatre theatre) {
    		this.theatre = theatre;
    }
    
    public void setTickets(List<Ticket> tickets) {
    		this.tickets = tickets;
    }
    
    public List<Ticket> getAllTickets() {
		return new ArrayList(tickets);
    }
}
