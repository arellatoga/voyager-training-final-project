package movietickets.domain.model;

import javax.persistence.*;

@Entity
@Table(name = "T_SEAT")
public class Seat {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "x")
    private int x;
    @Column(name = "y")
    private int y;
    
    @Column(name = "available")
    private boolean available;
    
    @Column(name = "occupied")
    private boolean occupied;
    
    @ManyToOne(cascade = {CascadeType.ALL})
    @JoinColumn(name = "theatre_id")
    private Theatre theatre;
    
    public Seat() {
    }
    
    public void setTheatre(Theatre theatre) {
    		this.theatre =  theatre;
    }
    
    public Theatre getTheatre() {
    		return theatre;
    }
    
    public void setX(int x) {
    		this.x = x;
    }
    
    public void setY(int y) {
		this.y = y;
    }

    public Seat(Theatre theatre, int x, int y) {
        this(theatre, x, y, true, false);
    }
    
    public Seat(Theatre theatre, int x, int y, boolean available) {
    		this(theatre, x, y, available, false);
    }

	public Seat(Theatre theatre, int x, int y, boolean available, boolean occupied) {
		this.theatre = theatre;
    		this.x = x;
    		this.y = y;
    		this.available = available;
    		this.occupied = occupied;
    }
	
	public void setOccupied(boolean occupied) {
		this.occupied = occupied;
	}
	
	public boolean getOccupied() {
		return occupied;
	}
	
	public boolean isOccupied() {
		return occupied;
	}
	
	public boolean getAvailable() {
		return available;
	}
	
	public void setAvailable(boolean available) {
		this.available = available;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (available ? 1231 : 1237);
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((theatre == null) ? 0 : theatre.hashCode());
		result = prime * result + x;
		result = prime * result + y;
		return result;
	}
	
	public int getX() {
		return x;
	}
	
	public int getY() {
		return y;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Seat other = (Seat) obj;
		if (theatre == null) {
			if (other.theatre != null)
				return false;
		} else if (!theatre.equals(other.theatre))
			return false;
		if (x != other.x)
			return false;
		if (y != other.y)
			return false;
		return true;
	}
}
