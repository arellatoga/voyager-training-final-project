package movietickets.domain.model.form;

public class TheatreTemplateWithSeatsForm {
	public int x;
	public int y;
	public String number;
	public String[] noSeats;

	public String[] getNoSeats() {
		return noSeats;
	}
	public void setNoSeats(String[] noSeats) {
		this.noSeats = noSeats;
	}
	public int getX() {
		return x;
	}
	public void setX(int x) {
		this.x = x;
	}
	public int getY() {
		return y;
	}
	public void setY(int y) {
		this.y = y;
	}
	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}
}
