package movietickets.domain.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.persistence.*;

@Entity
@Table(name = "T_MOVIE")
public class Movie {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long Id;

    @Column(name = "name")
    private String movieName;
    
    @Column(name = "runtime")
    private int runtime;
    
    // movie has many showings
    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "movie_id")
    private List<Showing> showings;

    public Movie() {
        // no-args Constructor
    }

    public Movie(String movieName, int runtime) {
        this.movieName = movieName;
        this.runtime = runtime;
        this.showings = new ArrayList<>();
    }
    
    public Long getId() {
		return Id;
    }

    public String getMovieName() {
        return movieName;
    }
    
    public int getRuntime() {
    		return runtime;
    }
    
    public List<Showing> getShowings() {
    		return showings;
    }

    public void setMovieName(String newName) {
        this.movieName = newName;
    }
    
    public void addShowing(Showing showing) {
    		this.showings.add(showing);
    }
}
